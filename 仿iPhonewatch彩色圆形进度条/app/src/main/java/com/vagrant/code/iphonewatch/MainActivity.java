package com.vagrant.code.iphonewatch;

import android.app.Activity;
import android.os.Bundle;

import com.vagrant.code.iphonewatch.view.CircleProgressBar;


public class MainActivity extends Activity {

    private CircleProgressBar circleBar1,circleBar2,circleBar3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        circleBar1 = (CircleProgressBar) findViewById(R.id.circle1);
        circleBar2 = (CircleProgressBar) findViewById(R.id.circle2);
        circleBar3 = (CircleProgressBar) findViewById(R.id.circle3);
        circleBar1.setMaxstepnumber(10000);      //设置一圈的最大值
        circleBar1.setColor(110, 211, 214);      //设置颜色
        circleBar2.setColor(247, 127, 150);
        circleBar3.setColor(244, 185, 66);
        int time = 1500;                        //动画时间
        circleBar1.update(40 * 100, time);
        circleBar2.update(30 * 100, time);
        circleBar3.update(50 * 100, time);
    }


}
